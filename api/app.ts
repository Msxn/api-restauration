import { ControllerAliments } from "./controllers/controllerAliments"
import { ControllerMenus } from "./controllers/controllerMenus"
import { ControllerAuthentication } from "./controllers/controllerAuthentication"
import swaggerUI from "swagger-ui-express";
import swaggerJsDocs from "swagger-jsdoc";

const mongoose = require('mongoose')
const express = require('express')
const jwt = require('jsonwebtoken');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');


/**
 * On créé une nouvelle "application" express
 */
const app = express()

/**
 * On dit à Express que l'on souhaite parser le body des requêtes en JSON
 *
 * @example app.post('/', (req) => req.body.prop)
 */

app.use(express.json());
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req,res,next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
})




//ROUTES-----------------
app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get('/', (req, res) => res.send('🏠'))

app.get('/menus/:token' , (req, res) => ControllerMenus.getMenus(req, res))
app.post('/menus/:token', (req, res) => ControllerMenus.postMenu(req, res))
app.get('/menus/:id/:token' , (req, res) => ControllerMenus.getMenuById(req, res))
app.delete('/menus/:id/:token', (req, res) => ControllerMenus.deleteMenuById(req, res))

app.get('/aliments/:token' , (req, res) => ControllerAliments.getAliments(req, res))
app.post('/aliments/:token' , (req, res) => ControllerAliments.postAliment(req, res))
app.get('/aliments/:id/:token' , (req, res) => ControllerAliments.getAlimentById(req, res))
app.get('/aliments/:id/:quantite/:token' , (req, res) => ControllerAliments.getAlimentDispo(req, res))
app.delete('/aliments/:id/:token' , (req, res)=> ControllerAliments.deleteAlimentById(req, res))

app.get('/tk/:token' , (req, res) => ControllerAuthentication.getAccess(req, res))

app.post("/signin", (req, res) => ControllerAuthentication.postToken(req,res))


app.listen(3001,()=>{
    "Serveur listening on port :3001"
})


async function main(){
    await mongoose.connect("mongodb://localhost:27017/test");
    console.log("console ok");
}

main().catch(err => console.log(err));





