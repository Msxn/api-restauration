import {Menus} from "../modeles/menus/menus";
import { ControllerAuthentication } from "./controllerAuthentication";

export class ControllerMenus{

    public static async getMenus(req, res){
        if(ControllerAuthentication.verifyToken(req.params.token)){
            let listeMenus = await Menus.getAllMenus();
            res.send(listeMenus);
        }else{
            res.status(401);
        }
    }
    public static async getMenuById(req, res){
        if(ControllerAuthentication.verifyToken(req.params.token)){
            let menu = await Menus.getOneMenu(req.params.id);
            res.send(menu);
        }else{
            res.status(401);
        }
    }

    public static async deleteMenuById(req, res){
        if(ControllerAuthentication.verifyToken(req.params.token)){
            let aliment = await Menus.getOneMenu(req.params.id);
            if(aliment != null)
            {
                await Menus.deleteOneMenu(req.params.id);
            }
            res.status(204);
            res.send();
        }else{
            res.status(401);
        }
    }

    public static async postMenu(req, res){
        if(ControllerAuthentication.verifyToken(req.params.token)){
            await Menus.postOneMenu(req.body);
            res.status(201);
            res.send();
        }else{
            res.status(401);
        }
    }

}