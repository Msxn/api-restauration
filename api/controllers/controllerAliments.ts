import {Aliments} from "../modeles/aliments/aliments";
import { ControllerAuthentication } from "./controllerAuthentication";

export class ControllerAliments{

    public static async getAliments(req, res){
        if(await ControllerAuthentication.verifyToken(req.params.token) === true){
            let listeAliments = await Aliments.getAllAliments();
            res.send(listeAliments);
        }else{
            res.status(401);
        }
    }
    public static async getAlimentById(req, res){
        if(ControllerAuthentication.verifyToken(req.params.token)){
            let aliment = await Aliments.getOneAliment(req.params.id);
            res.send(aliment);
        }else{
            res.status(401);
        }
        
    }
    public static async getAlimentDispo(req, res){
        if(ControllerAuthentication.verifyToken(req.params.token)){
            let aliment = await Aliments.getDispoAliment(req.params.id, req.params.quantite);
            res.send(aliment);
        }else{
            res.status(401);
        }
    }

    public static async deleteAlimentById(req, res){
        if(ControllerAuthentication.verifyToken(req.params.token)){
            let aliment = await Aliments.getOneAliment(req.params.id);
            if(aliment != null)
            {
                await Aliments.deleteOneAliment(req.params.id);
            }
            res.status(204);
            res.send();
        }else{
            res.status(401);
        }
    }

    public static async postAliment(req, res){
        if(ControllerAuthentication.verifyToken(req.params.token)){
            await Aliments.postOneAliment(req.body);
            res.status(201);
            res.send();
        }else{
            res.status(401);
        }
    }

}