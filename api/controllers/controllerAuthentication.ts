import {Token} from "../modeles/token/token";

export class ControllerAuthentication{

    public static async postToken(req, res){
        await Token.postOneToken(req.body);
        res.status(201);
        res.send();
    }

    public static async getAccess(req, res){
        let verify = await this.verifyToken(req.params.token)
        res.send(verify)
    }

    public static async verifyToken(tok){
        let token = await Token.getCredentials(tok);
        return token
    }

}