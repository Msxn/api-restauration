import { Schema } from "mongoose";

const mongoose = require('mongoose');

export const alimentSchema= new Schema({
    nom: String, 
    type: String,
    quantite: String,
    date: { 
        type: Date, 
        default: Date.now
    },
});

const AlimentModel= mongoose.model('Aliments', alimentSchema);

export class Aliments{

    public static async getAllAliments():Promise<any>{
        return new Promise(async (resolve)=>{
            resolve(await AlimentModel.find());
        })
    }

    public static async getOneAliment(id:string):Promise<any>{
        return new Promise(async (resolve)=>{
            resolve(await AlimentModel.findOne({_id:id}));
        })
    }

    public static async getDispoAliment(id:string, quantite:Number):Promise<any>{
        let aliment = await AlimentModel.findOne({_id:id})
        if(aliment.quantite >= quantite){
            return true
        }else{
            return false
        }
    }

    public static async deleteOneAliment(id:string):Promise<any>{
        return await AlimentModel.findByIdAndRemove({_id : id});
    }

    public static async postOneAliment(body: { nom: String, type: String, quantite: String } )
    {
        const aliment = new AlimentModel(
            {
                nom: body.nom, 
                type: body.type,
                quantite: body.quantite,
                date: new Date()
            }
        );
            
        return await aliment.save();


    }

}
