import { Schema } from "mongoose";

const mongoose = require('mongoose');

export const menuSchema = new Schema({
    nom : String,
    sandwich : {
        nom : String,
        viande : String,
        aliments : [{nom:String, id_aliment:String, quantite:Number}]
    },
    accompagnement : String,
    boisson : String,
    sauces : [{nom:String, quantite:String}],
    prix : Number
})

const menuModel = mongoose.model('Menus' , menuSchema);

export class Menus{

    public static async getAllMenus():Promise<any>{
        return new Promise(async (resolve)=>{
            resolve(await menuModel.find());
        })
    }

    public static async getOneMenu(id:string):Promise<any>{
        return new Promise(async (resolve)=>{
            resolve(await menuModel.findOne({_id : id}));
        })
    }

    public static async deleteOneMenu(id:string):Promise<any>{
        return await menuModel.findByIdAndRemove({_id : id});
    }


    public static async postOneMenu(
        body : 
        {
            nom : String,
            sandwich : { nom : String , viande : String , aliments : [{ nom:String, id_aliment:String,  quantite:Number }] },
            accompagnement : String,
            boisson : String,
            sauces : [{ nom:String , quantite:String }],
            prix : Number
        }) //---------------------------
    {
        const menu = new menuModel({
            nom : body.nom,
            sandwich : body.sandwich,
            accompagnement : body.accompagnement,
            boisson : body.boisson,
            sauces : body.sauces,
            prix : body.prix
        });

        return await menu.save();
    }

}
