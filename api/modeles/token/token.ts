import { Schema } from "mongoose";

const mongoose = require('mongoose');

export const tokenSchema= new Schema({
    token: String, 
});

const TokenModel= mongoose.model('Tokens', tokenSchema);

export class Token{

    public static async getCredentials(tokenGiven:string):Promise<any>{
        let tokenSearch = await TokenModel.findOne({token:tokenGiven})
        if(tokenSearch.token === tokenGiven){
            return true
        }else{
            return false
        }
    }

    public static async postOneToken( body : {token : String} ) 
    {
        const tokenModel = new TokenModel({
            token : body.token
        });
        return await tokenModel.save();
    }

}