import React            from 'react'
import { Button, Form } from 'react-bootstrap'
import {Credentials}  from '../key'

export default function Login() {

  return (
    <Form>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control type="email" placeholder="exemple@exemple.com" />
        <Form.Text className="text-muted">
          Veuillez vous connecter avec votre adresse mail complète
        </Form.Text>
      </Form.Group>

      <Form.Group className="mb-3" controlId="formBasicPassword">
        <Form.Label>Mot de passe</Form.Label>
        <Form.Control type="password" placeholder="Abc1234?!" />
      </Form.Group>

      <Button variant="primary" type="submit">
        Se Connecter
      </Button>
    </Form>
  )
}
