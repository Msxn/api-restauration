import React        from 'react'
import Container    from 'react-bootstrap/Container';
import Nav          from 'react-bootstrap/Nav';
import Navbar       from 'react-bootstrap/Navbar';
import {Link}       from 'react-router-dom'


export default function MainNavbar() {
  return (
    <div>
        <Navbar bg="light" variant="light">
            <Container>
                <Navbar.Brand href="/">Navbar</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <Link class="nav-link" to="/"><b>Accueil</b></Link>
                        <Link class="nav-link" to="/menus/add">Menus</Link>
                        <Link class="nav-link">A propos</Link>
                    </Nav>
                    <Nav>
                        <Link class="nav-link" to="/login">Se Connecter</Link>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        <br />
    </div>
  )
}
