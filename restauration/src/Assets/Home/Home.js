import React            from 'react';
import axios            from 'axios';

import Container        from 'react-bootstrap/Container';
import Row              from 'react-bootstrap/Row';
import Col              from 'react-bootstrap/Col';
import Button           from 'react-bootstrap/Button';
import Card             from 'react-bootstrap/Card';

import {Credentials}    from '../key'
import { useNavigate }  from 'react-router-dom';


import './Home.css';

const APIUrl = "http://localhost:3001";


export default function Home() {

  const [plats, setPlats] = React.useState(null);
  const [aliments, setAliments] = React.useState(null);
  const navigate = useNavigate();


  const handleCommand = (event) => {
    alert(event.currentTarget.id)
  }

  const handleDelete = (event) => {
    
    fetch(`${APIUrl}/menus/${event.currentTarget.id}/`+ Credentials(), { method: 'DELETE' }).then(navigate("/"))

    

  }

  React.useEffect(() => {

    async function fetchPlats(){
      return await axios.get(`${APIUrl}/menus/`+Credentials())
    }

    async function fetchAliments(){
      return await axios.get(`${APIUrl}/aliments/`+Credentials())
    }

    (async () => {
      const platsList = await fetchPlats();
      setPlats(platsList.data);
      const alimentsList = await fetchAliments();
      setAliments(alimentsList.data);
      
    })();

  }, []);

  let dispo = true;

  if (!plats || !aliments) return null;

  return (
    
      <Container>
        <Row className="justify-content-md-center">
          {
              plats.map
              (
                (plat) => 
                (
                  <Col xs lg="3">
                    <Card style={{ width: '18rem' }}>
                      <Card.Img variant="top" src="/double_whooper.jpg" />
                      <Card.Body>
                        <Card.Title>{plat.nom}</Card.Title>
                        <Card.Text>
                          Burger King te propose un menu spécial {plat.sandwich.nom}. 
                          Ingrédients :
                          <ul>
                            {
                              plat.sandwich.aliments.map(
                                (aliment) => (
                                  <li>{aliment.nom} : {aliment.quantite}</li>
                                )
                              )
                            }
                          </ul>
                            
                          <h3>
                            {plat.prix} € le menu
                          </h3>
                        </Card.Text>
                        {
                          plat.sandwich.aliments.map((aliment) => 
                            {
                              aliments.map((a) => {
                                a.nom === aliment.nom ? a.quantite < aliment.quantite ? dispo = false : <p></p> : <p></p>
                              })
                            }
                          )
                        }

                        {dispo === true ? 
                          <Button variant="primary" id={plat._id} onClick={handleCommand}>Commander</Button> :
                          <Button disabled variant="secondary">Non disponible</Button>
                        }
                        <Button variant="danger" id={plat._id} onClick={handleDelete}>Supprimer</Button>
                      </Card.Body>
                    </Card>
                  </Col>
                )
              )
            }
        </Row>
        <Row>

        </Row>
      </Container>
      
      
    
  );
}


