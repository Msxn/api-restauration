import axios                from 'axios';
import React, {useState}    from 'react'
import {Button, 
        Form,
        Row, Col,
        Container }         from 'react-bootstrap'
import Select               from 'react-select'
import { Credentials }      from '../key'
import { useNavigate }           from 'react-router-dom';


export default function AddMenu() {

    const APIUrl = "http://localhost:3001";


    const [alimentList, setAlimentList] = useState([]);
    const [saucesList, setSaucesList] = useState();
    const [boissonsList, setBoissonsList] = useState();
    const navigate = useNavigate();


    const boissons = [
        { value: '33cl', label: 'Canette 33cl' },
        { value: '50cl', label: 'Cup 50cl' },
        { value: '1L', label: 'Bouteille 1L' }
    ]

    const sauces = [
        { value: 'mayonnaise', label: 'Mayonnaise' },
        { value: 'ketchup', label: 'Ketchup' },
        { value: 'curry', label: 'Curry' },
        { value: 'blanche', label: 'Blanche' },
        { value: 'burger', label: 'Biggy Burger' }
    ]

        
    const [inputs, setInputs] = useState({
        nom : "",
        sandwich : {
            nom : "",
            viande : "",
            aliments : Array.from(alimentList)
        },
        accompagnement : "",
        boisson : "",
        sauces : [],
        prix : ""
    });


    const handleChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setInputs(values => ({...values, [name]: value}))
    }

    const handleAlimentsChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        setAlimentList(values => ({...values, [name]: value}))
    }

    function handleSaucesChange(data) {
        setSaucesList(data);
    }
    
    function handleBoissonsChange(data) {
        setBoissonsList(data);
    }


    const handleSubmit = (event) => {
        event.preventDefault();

        const aliments = [];
        const sauces = [];
        const boisson = "";

        Object.keys(alimentList).forEach(element => {
            aliments.push({nom : element , quantite : alimentList[element]});
        })

        Object.keys(saucesList).forEach(element => {
            sauces.push({[element] : saucesList[element].label});
        })

        Object.keys(boissonsList).forEach(element => {
            boissons.push({element : boissonsList[element]});
        })

        console.log(aliments)

        const menu = {
            nom : ("Menu " + inputs.menuNom),
            sandwich : {
                nom : inputs.menuNom,
                viande : inputs.sandwichViande,
                aliments : aliments
            },
            accompagnement : inputs.accompagnement,
            boisson : boisson,
            sauces : sauces,
            prix : inputs.prix
        }

        fetch(`${APIUrl}/menus/`+ Credentials(), {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(menu)
        }).then(navigate('/'))
        

      }

    return (
        <Row>
            <Col lg="7">

                <h1>Ajouter un menu</h1><br/>

                <Form onSubmit={handleSubmit}>
                    <Form.Group className="mb-3">
                        <Form.Label>Nom du menu</Form.Label>
                        <Form.Control type="text" name="menuNom" placeholder="" value={inputs.menuNom || ""}
                            onChange={handleChange} />
                        <Form.Text className="text-muted">
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Viande</Form.Label>
                        <Form.Control type="text" name="sandwichViande" placeholder="" value={inputs.sandwichViande || ""} 
                            onChange={handleChange} />
                        <Form.Text className="text-muted">
                        </Form.Text>
                    </Form.Group>


                    <Container>
                        <Row>
                            <Col>
                                <Form.Group className="sm-3">
                                    <Form.Label>Salade</Form.Label>
                                    <Form.Control type="number" name="salade" placeholder="0"  value={alimentList.salade || ""} 
                                        onChange={handleAlimentsChange} />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="sm-3">
                                    <Form.Label>Tomate</Form.Label>
                                    <Form.Control type="number" name="tomate" placeholder="0"  value={alimentList.tomate || ""} 
                                        onChange={handleAlimentsChange}  />
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="sm-2">
                                    <Form.Label>Oignon</Form.Label>
                                    <Form.Control type="number" name="oignon" placeholder="0"  value={alimentList.oignon || ""} 
                                        onChange={handleAlimentsChange} />
                                </Form.Group>
                            
                            </Col>
                            <Col>
                                <Form.Group className="sm-2">
                                    <Form.Label>Bacon</Form.Label>
                                    <Form.Control type="number" name="bacon" placeholder="0" value={alimentList.bacon || ""} 
                                        onChange={handleAlimentsChange}  />
                                </Form.Group>
                            </Col>
                        </Row>
                    </Container>

                    <br/>

                    <Form.Group className="mb-3">
                        <Form.Label>Description</Form.Label>
                        <Form.Control as="textarea" name="menuDesc" placeholder="" value={inputs.menuDesc || ""} 
                            onChange={handleChange} />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Accompagnement</Form.Label>
                        <Form.Control type="text" name="accompagnement" placeholder="" value={inputs.accompagnement || ""} 
                            onChange={handleChange} />
                        <Form.Text className="text-muted">
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Image du menu</Form.Label>
                        <Form.Control type="file" name="menuImage" accept=".jpg,.png" />
                    </Form.Group>

                    <Container>
                        <Row>
                            <Col>
                                <Form.Group className="sm-2">
                                    <Form.Label>Sauce(s)</Form.Label>
                                    <Select isMulti options={sauces} 
                                        name="sauces" placeholder="" onChange={handleSaucesChange} value={saucesList} />
                                    <Form.Text className="text-muted">
                                    </Form.Text>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="sm-2">
                                    <Form.Label>Boisson(s)</Form.Label>
                                    <Select options={boissons} 
                                        name="boissons" placeholder="" onChange={handleBoissonsChange} value={boissonsList} />
                                    <Form.Text className="text-muted">
                                    </Form.Text>
                                </Form.Group>
                            </Col>
                            <Col>
                                <Form.Group className="sm-2">
                                    <Form.Label>Prix</Form.Label>
                                    <Form.Control type="text" name="prix" placeholder="" value={inputs.prix || ""} 
                                        onChange={handleChange} />
                                    <Form.Text className="text-muted">
                                    </Form.Text>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Container>

                    <br />

                    <Button variant="primary" type="submit">
                        Enregistrer
                    </Button>


                </Form>
            </Col>
            <Col lg="5">
                
            </Col>
        </Row>
    )
}
