import React             from 'react';
import Home              from './Assets/Home/Home';
import Login             from './Assets/Login/Login';
import { BrowserRouter, 
          Routes, 
          Route }        from "react-router-dom";
import MainNavbar        from './Assets/Navbar/MainNavbar';
import Container         from 'react-bootstrap/Container';
import AddMenu           from './Assets/Menus/AddMenu';


export default function App() {
  return (
      <BrowserRouter>
        <MainNavbar />
        <Container>
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/login" element={<Login />} />
          <Route path="/menus/add" element={<AddMenu />} />
        </Routes>
        </Container>
      </BrowserRouter>
  );
}

